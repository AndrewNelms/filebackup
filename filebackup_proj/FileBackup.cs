﻿using System;
using System.Windows.Forms;
using System.IO;

namespace filebackup_proj
{
    public partial class FileBackup : Form
    {
        public FileBackup(string[] Args)
        {
            InitializeComponent();
            if (string.Join("", Args) == "/s" && Args.Length > 0) //If there are arguments, it will allow it to run as a console application
            {
                BackupFiles();
                Environment.Exit(1);//Exits program before showing GUI
            }
            else if (Args.Length == 0)
            {
            } // This is if there is no arguments passed it will open the form. C# will automatically exit loop when condition is met, moving to the next section of code. Since it is after the InitializeComponent section, the form will load
            else
            {
               //Implement e-mail warning about the arguments
            }
        }

        private void Destination_Load(object sender, EventArgs e)
        {
            currentLbl.Text = "Current Backup directory is: " + Properties.Settings.Default.BackupDestination.ToString() + "Current source directory is: " + Properties.Settings.Default.SourceFolder.ToString();
            backupLbl.Text = Properties.Settings.Default.BackupDestination.ToString();
        }

        //Constants to be used throught the program
        const string errorLog = @"C:\fileBackup\errorLog.txt"; //Error log: Referenced for when shit hits the fan
        const string resultLog = @"C:\fileBackup\results.txt"; //Result log: Referenced to view the results of what has ran, such as logs, ets

        //write error log
        public void WriteErrorLog(string err)
        {
            if (!File.Exists(errorLog)) //insures we have a file to write to
            {
                var sw = File.CreateText(errorLog);
                sw.Close();
            }

            try // tries to write the file
            {
                var thisDay = DateTime.Today;
                var date = thisDay.ToString("d");
                date = date.Replace("/", "-");

                using (StreamWriter sw = File.AppendText(errorLog))  //adds to the error log
                {
                    sw.WriteLine(date + ": " + err);
                }
            }
            catch (Exception ex) //what happens if it dont write to good
            {
                //Email alert to be added at later date
                return; //exits the method 
            }
        }

        public void WriteResultLog(string result)
        {
            if (!File.Exists(resultLog)) //Creates log if it doesn't exist
            {
                var sw = File.CreateText(resultLog);
                sw.Close();
            }

            try
            {
                var thisDay = DateTime.Today; //Gets todays date
                var date = thisDay.ToString("d"); //Sets date to mm/dd/yy fromat
                date = date.Replace("/", "-");  // replaces / with - because - is prettier than /. Unless your into that sort of thing

                using (StreamWriter sw = File.AppendText(resultLog))  //adds to the error log
                {
                    sw.WriteLine(date + ": " + result); // pretty self exlplainable, puts the date, a space, and the result. Reult is sent when method is called
                }
                return;
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void bBtn_Click(object sender, EventArgs e)
        {
            string backup;
            var fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                backupLbl.Text = fbd.SelectedPath.ToString();
                backup = fbd.SelectedPath;
                Properties.Settings.Default.BackupDestination = backup;
            }
        }

        private void srcBtn_Click(object sender, EventArgs e)
        {
            string backup;

            var fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                backupLbl.Text = fbd.SelectedPath.ToString();
                backup = fbd.SelectedPath;
                Properties.Settings.Default.SourceFolder= backup;
            }
        }

        private void backupBtn_Click(object sender, EventArgs e)
        {
            BackupFiles();
        }

        private void BackupFiles()
        {
            //Create Backup Folder
            var thisDay = DateTime.Today;
            var date = thisDay.ToString("d"); //Create date in format MM/DD/YYYY
            date = date.Replace("/", "-"); //Changes MM/DD/YYY to MM-DD-YYYY so that it doesn't break Windows folder conventions
            string directory = Properties.Settings.Default.BackupDestination + "\\Temp" + "\\CNCBackup" + date;
            Directory.CreateDirectory(directory); //Creates backup folder with todays date

            //Copy files to the destination folder


        }

        private void MailService()
        {
            
        }
    }
}
