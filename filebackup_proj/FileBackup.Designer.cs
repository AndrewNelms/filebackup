﻿namespace filebackup_proj
{
    partial class FileBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backupLbl = new System.Windows.Forms.Label();
            this.bBtn = new System.Windows.Forms.Button();
            this.currentLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.srcBtn = new System.Windows.Forms.Button();
            this.srcLbl = new System.Windows.Forms.Label();
            this.backupBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // backupLbl
            // 
            this.backupLbl.AutoSize = true;
            this.backupLbl.Location = new System.Drawing.Point(118, 42);
            this.backupLbl.Name = "backupLbl";
            this.backupLbl.Size = new System.Drawing.Size(105, 13);
            this.backupLbl.TabIndex = 16;
            this.backupLbl.Text = "Destination Directory";
            // 
            // bBtn
            // 
            this.bBtn.Location = new System.Drawing.Point(305, 42);
            this.bBtn.Name = "bBtn";
            this.bBtn.Size = new System.Drawing.Size(115, 23);
            this.bBtn.TabIndex = 14;
            this.bBtn.Text = "Browse Backup";
            this.bBtn.UseVisualStyleBackColor = true;
            this.bBtn.Click += new System.EventHandler(this.bBtn_Click);
            // 
            // currentLbl
            // 
            this.currentLbl.Location = new System.Drawing.Point(25, 113);
            this.currentLbl.Name = "currentLbl";
            this.currentLbl.Size = new System.Drawing.Size(371, 69);
            this.currentLbl.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Desstination Folder:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Source Folder:";
            // 
            // srcBtn
            // 
            this.srcBtn.Location = new System.Drawing.Point(305, 8);
            this.srcBtn.Name = "srcBtn";
            this.srcBtn.Size = new System.Drawing.Size(115, 23);
            this.srcBtn.TabIndex = 18;
            this.srcBtn.Text = "Browse Source";
            this.srcBtn.UseVisualStyleBackColor = true;
            this.srcBtn.Click += new System.EventHandler(this.srcBtn_Click);
            // 
            // srcLbl
            // 
            this.srcLbl.AutoSize = true;
            this.srcLbl.Location = new System.Drawing.Point(94, 13);
            this.srcLbl.Name = "srcLbl";
            this.srcLbl.Size = new System.Drawing.Size(86, 13);
            this.srcLbl.TabIndex = 19;
            this.srcLbl.Text = "Source Directory";
            // 
            // backupBtn
            // 
            this.backupBtn.Location = new System.Drawing.Point(168, 75);
            this.backupBtn.Name = "backupBtn";
            this.backupBtn.Size = new System.Drawing.Size(75, 23);
            this.backupBtn.TabIndex = 20;
            this.backupBtn.Text = "Backup Now";
            this.backupBtn.UseVisualStyleBackColor = true;
            this.backupBtn.Click += new System.EventHandler(this.backupBtn_Click);
            // 
            // FileBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 191);
            this.Controls.Add(this.backupBtn);
            this.Controls.Add(this.srcLbl);
            this.Controls.Add(this.srcBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.backupLbl);
            this.Controls.Add(this.bBtn);
            this.Controls.Add(this.currentLbl);
            this.Controls.Add(this.label1);
            this.Name = "FileBackup";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label backupLbl;
        private System.Windows.Forms.Button bBtn;
        private System.Windows.Forms.Label currentLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button srcBtn;
        private System.Windows.Forms.Label srcLbl;
        private System.Windows.Forms.Button backupBtn;
    }
}

